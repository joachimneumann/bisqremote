# bisq-remote
Mobile notifications from the Bisq-Desktop App and remote control

## Documentation
The documentation can be found in [issue 25](https://github.com/bisq-network/proposals/issues/25) of the bisq-network/proposals repository.
